import PropTypes from 'prop-types';
import Header from '../Header';
import Footer from '../Footer';

const Layout = ({ children, home }) => (
  <>
    {home ? (
      <div className='flex flex-10a flex-col m-auto max-w-95 pt-8 sm:justify-center sm:pt-0'>
        <Header home />
        {children}
      </div>
    ) : (
      <div>
        <Header />
        <div className='flex flex-10a flex-col justify-center m-auto pt-8 w-95 sm:pt-0 lg:w-60'>{children}</div>
      </div>
    )}
    <Footer />
  </>
);

Layout.defaultProps = {
  home: false
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  home: PropTypes.bool
};

export default Layout;
