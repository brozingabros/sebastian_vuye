import PropTypes from 'prop-types';
import Link from 'next/link';
import avatar from '../../constants';

const Header = ({ home }) => (
  <>
    {home ? (
      <header>
        <Link href='/'>
          <a className='items-center flex flex-col justify-center w-full'>
            <div className='h-32 relative w-40 w-full sm:h-40'>
              <img
                src={avatar.image.placeholder.default.trace}
                alt={avatar.image.placeholder.alt}
                className='rounded-full left-0 right-0 top-0 ml-auto mr-auto absolute text-center w-28 sm:w-40'
              />
              <picture className='rounded-full left-0 right-0 top-0 ml-auto mr-auto absolute text-center w-28 sm:w-40'>
                <source
                  srcSet={avatar.image.home.webP.srcSet}
                  type='image/webp'
                  className='rounded-full'
                  sizes='(min-width: 640px) 224px, (min-width: 1280px) 320px, 112px'
                />
                <source
                  srcSet={avatar.image.home.default.srcSet}
                  type='image/jpeg'
                  className='rounded-full'
                  sizes='(min-width: 640px) 224px, (min-width: 1280px) 320px, 112px'
                />
                <img
                  alt={avatar.alt}
                  src={avatar.image.home.default.src}
                  sizes='(min-width: 640px) 224px, (min-width: 1280px) 320px, 112px'
                  className='rounded-full'
                />
              </picture>
            </div>

            <h1 className='font-title text-5xl font-bold my-4 text-center text-primary uppercase sm:text-7xl'>
              Sebastian <span className='block sm:inline'>Vuye</span>
            </h1>
          </a>
        </Link>
      </header>
    ) : (
      <header>
        <Link href='/'>
          <a className='items-center flex justify-start m-auto max-w-95 py-4 lg:max-w-60'>
            <div className='h-14 relative w-full sm:h-10'>
              <img
                src={avatar.image.placeholder.default.trace}
                alt={avatar.image.placeholder.alt}
                className='rounded-full left-0 top-0 ml-auto mr-auto absolute text-center w-14 sm:w-10'
              />
              <picture className='rounded-full left-0 top-0 mb-auto mt-auto absolute w-14 sm:w-10'>
                <source
                  srcSet={avatar.image.alternative.webP.srcSet}
                  type='image/webp'
                  className='rounded-full'
                  sizes='(min-width: 640px) 40px, 56px'
                />
                <source
                  srcSet={avatar.image.alternative.default.srcSet}
                  type='image/jpeg'
                  className='rounded-full'
                  sizes='(min-width: 640px) 40px, 56px'
                />
                <img
                  alt={avatar.alt}
                  src={avatar.image.alternative.default.src}
                  sizes='(min-width: 640px) 40px, 56px'
                  className='rounded-full'
                />
              </picture>
              <h1 className='font-title text-3xl font-bold bottom-0 left-16 top-0 leading-3 m-auto absolute text-primary uppercase w-auto sm:text-2xl sm:left-14'>
                <span className='block top-1/2 relative transform -translate-y-1/2'>Sebastian Vuye</span>
              </h1>
            </div>
          </a>
        </Link>
      </header>
    )}
  </>
);

Header.defaultProps = {
  home: false
};

Header.propTypes = {
  home: PropTypes.bool
};

export default Header;
