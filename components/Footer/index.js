import { FaHeart } from 'react-icons/fa';

const socialNavigation = [
  { id: 1, text: 'bitbucket', url: 'https://bitbucket.org/brozingabros/' },
  { id: 2, text: 'instagram', url: 'https://www.instagram.com/sebavuye/' },
  { id: 3, text: 'linkedin', url: 'https://www.linkedin.com/in/sebastian-vuye' }
];

const Footer = () => (
  <footer className='items-center flex flex-col flex-shrink-0 m-auto max-w-95 py-2'>
    <ul className='flex'>
      {socialNavigation.map(item => (
        <li key={item.id}>
          <a
            className='text-body lg:hover:text-accent text-primary uppercase duration-300 transition ease-in-out'
            href={item.url}
            rel='noreferrer'
            target='_blank'>
            {item.text}
          </a>
          {socialNavigation[socialNavigation.length - 1] !== item && <span className='px-1 text-accent'>&mdash;</span>}
        </li>
      ))}
    </ul>
    <p className='text-sm my-2 text-center text-primary'>
      <span className='block sm:inline'>
        Made with <FaHeart className='inline text-accent' role='img' aria-label='Golden heart icon' /> in Antwerp{' '}
        <span className='hidden sm:inline'>&middot;</span>
      </span>
      <span className='block sm:inline'>
        {' '}
        Powered by Next.js and tailwindcss <span className='hidden sm:inline'>&middot;</span>
      </span>
      <span className='block sm:inline'> &copy; Sebastian Vuye</span>
    </p>
  </footer>
);

export default Footer;
