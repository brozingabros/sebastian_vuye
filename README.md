# sebastian_vuye

[![Netlify Status](https://api.netlify.com/api/v1/badges/62cfb9d9-94c6-41dd-8f0a-4ac1bbc6619a/deploy-status)](https://app.netlify.com/sites/sebastianvuye/deploys)

My personal portfolio [website](https://www.sebastianvuye.be) written in **Next.js** and **Tailwindcss**.

## Installation

Use the package manager `yarn` or `npm` to install and run.

```bash
yarn install
```

## Usage

To start the development server run:

```bash
yarn dev
```
