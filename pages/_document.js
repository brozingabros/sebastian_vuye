import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang='en'>
        <Head>
          {/* general */}
          <meta name='title' content='Sebastian Vuye | Front-End Developer & Photographer' />
          <meta
            name='description'
            content='My name is Sebastian Vuye, I’m a front-end developer and a lifestyle photographer.'
          />
          <meta name='keywords' content='web, front-end, photographer' />
          <meta httpEquiv='Content-Type' content='text/html; charset=utf-8' />
          <meta name='language' content='English' />
          <link rel='apple-touch-icon' sizes='180x180' href='/apple-touch-icon.png ' />
          <link rel='icon' type='image/png' sizes='32x32' href='/favicon-32x32.png' />
          <link rel='icon' type='image/png' sizes='16x16' href='/favicon-16x16.png' />
          <link rel='manifest' href='/site.webmanifest' />
          <link rel='mask-icon' href='/safari-pinned-tab.svg' color='#5bbad5' />
          <meta name='apple-mobile-web-app-title' content='SV' />
          <meta name='application-name' content='SV' />
          <meta name='msapplication-TileColor' content='#b91d47' />
          <meta name='theme-color' content='#ffffff' />
          {/* social */}
          <meta property='og:image:width' content='279' />
          <meta property='og:image:height' content='279' />
          <meta property='og:description' content='I&rsquo;m a front-end developer and a lifestyle photographer.' />
          <meta property='og:title' content='Sebastian Vuye | Front-End Developer &amp; Photographer' />
          <meta property='og:image' content='https://www.sebastianvuye.be/og-image.jpg' />
          <meta property='og:url' content='www.sebastianvuye.be' />
          <meta name='twitter:card' content='summary' />
          <meta name='twitter:site' content='@sebavuye' />
          <meta name='twitter:title' content='Sebastian Vuye | Front-End Developer &amp; Photographer' />
          <meta name='twitter:description' content="I'm a front-end developer and a lifestyle photographer." />
          <meta name='twitter:image' content='https://www.sebastianvuye.be/og-image.jpg' />
          {/* fonts */}
          <link rel='preconnect' href='https://fonts.gstatic.com/' crossOrigin='true' />
          <link
            rel='stylesheet'
            href='https://fonts.googleapis.com/css?family=Cabin|Roboto:400,700|Roboto+Condensed:400,700&display=swap'
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
