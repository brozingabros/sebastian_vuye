import Head from 'next/head';
import Layout from '../components/Layout';

const About = () => (
  <Layout>
    <Head>
      <title>SV - About</title>
    </Head>

    <main className='pb-20 sm:py-20'>
      <h2 className='font-title text-4xl font-bold pt-5 text-primary capitalize'>about</h2>
      <span className='border-accent border-b-2 block py-2 w-14' />
      <article className='py-10'>
        <h3 className='font-title text-2xl pb-4 text-primary uppercase'>hello</h3>
        <p className='font-body tracking-wide tracking-wide leading-loose leading-loose py-6 text-primary'>
          My name is Sebastian and I’m a front-end developer. I build and deploy web interfaces from the ground up. I’m
          a hard worker; and love learning and researching new technologies. Currently; my main focus is Javascript and
          React. The past two years; I have worked as a consultant in web development. In my spare time, I am always
          working on interesting projects on the side. Currently working as a consultant at{' '}
          <a
            className='lg:hover:text-accent border-accent border-b-2 text-primary duration-300 transition ease-in-out'
            href='https://www.telenet.be'
            target='_blank'
            rel='noreferrer'>
            Telenet.
          </a>
        </p>
        <hr className='border-1 border-accent' />
      </article>
      <article className='pb-10'>
        <h3 className='font-title text-2xl pb-4 text-primary uppercase'>used technologies</h3>
        <ul className='font-body py-6'>
          <li className='pl-4'>
            <span className='text-accent'>&mdash;</span>
            <span className='inline pl-2 text-primary' role='listitem'>
              React
            </span>
          </li>
          <li className='pl-4'>
            <span className='text-accent'>&mdash;</span>
            <span className='inline pl-2 text-primary' role='listitem'>
              JavaScript
            </span>
          </li>
          <li className='pl-4'>
            <span className='text-accent'>&mdash;</span>
            <span className='inline pl-2 text-primary' role='listitem'>
              SCSS / CSS
            </span>
          </li>
          <li className='pl-4'>
            <span className='text-accent'>&mdash;</span>
            <span className='inline pl-2 text-primary' role='listitem'>
              HTML
            </span>
          </li>
        </ul>
        <hr className='border-1 border-accent' />
      </article>
      <article className='pb-10'>
        <h3 className='font-title text-2xl pb-4 text-primary uppercase'>projects</h3>
        <section className='flex flex-col font-body py-6'>
          <section className='flex flex-col tracking-wide leading-loose pb-8 sm:pb-16'>
            <h4 className='font-title text-lg pb-4 text-primary lowercase'>main</h4>
            <ul>
              <li className='pb-16 pl-4'>
                <section className='flex'>
                  <span className='text-accent'>&mdash;</span>
                  <p className='font-bold pb-1 pl-2 text-primary'>
                    Webmaster at{' '}
                    <a
                      className='lg:hover:text-accent border-accent border-b-2 text-primary duration-300 transition ease-in-out'
                      href='https://www.telenet.be'
                      rel='noreferrer'
                      target='_blank'>
                      Telenet
                    </a>{' '}
                    -{' '}
                    <a
                      className='lg:hover:text-accent border-accent border-b-2 text-primary duration-300 transition ease-in-out'
                      href='https://www.base.be'
                      rel='noreferrer'
                      target='_blank'>
                      BASE
                    </a>{' '}
                    <span className='block font-normal text-primary'>02/2020 - now</span>
                  </p>
                </section>
                <ul>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Implementation of innovative ideas and customer driven enhancements in{' '}
                        <span className='font-bold'>Adobe Experience Manager</span>.
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p>
                        Setting up <span className='font-bold'>A/B tests in Adobe Target</span>.
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        <span className='font-bold'>Guiding and coaching</span> the BASE authoring squad.
                      </p>
                    </section>
                  </li>
                </ul>
              </li>
              <li className='pb-8 pl-4'>
                <section className='flex'>
                  <span className='text-accent'>&mdash;</span>
                  <p className='font-bold pb-2 pl-2 text-primary'>
                    Webmaster at Colruyt Group -{' '}
                    <a
                      className='lg:hover:text-accent border-accent border-b-2 text-primary duration-300 transition ease-in-out'
                      href='https://www.collishop.be/'
                      rel='noreferrer'
                      target='_blank'>
                      Collishop
                    </a>{' '}
                    <span className='block font-normal'>04/2019 - 02/2020</span>
                  </p>
                </section>
                <ul>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        <span className='font-bold'>Maintaining</span> day-to-day operations at the Collishop website.
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        <span className='font-bold'>Automating</span> author processes by creating templates and custom
                        <span className='font-bold'> gulp builds</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Converting marketing campaigns to digital landing pages in{' '}
                        <span className='font-bold'>HTML</span>, <span className='font-bold'>SCSS</span> and{' '}
                        <span className='font-bold'>JavaScript</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Creating assets in <span className='font-bold'>Figma</span> or{' '}
                        <span className='font-bold'>Sketch</span>
                      </p>
                    </section>
                  </li>
                </ul>
              </li>
            </ul>
          </section>

          <section className='flex flex-col tracking-wide leading-loose pb-8 sm:pb-0'>
            <h4 className='font-title text-lg pb-4 text-primary lowercase'>side</h4>
            <ul className='flex flex-col flex-wrap sm:flex-row'>
              <li className='pb-16 pl-4 w-full sm:w-1/2'>
                <section className='flex'>
                  <span className='text-accent'>&mdash;</span>
                  <a
                    className='projects lg:hover:text-accent font-bold pl-2 text-primary duration-300 transition ease-in-out'
                    href='https://develop--stormweatherapp.netlify.app/'
                    rel='noreferrer'
                    target='_blank'>
                    Weather App - WIP
                    <span className='arrow' />
                  </a>
                </section>
                <ul>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Build with <span className='font-bold'>React (CRA)</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Styled with <span className='font-bold'>material-ui</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Tested with <span className='font-bold'>React Testing Library</span> (integration)
                      </p>
                    </section>
                  </li>
                </ul>
              </li>
              <li className='pb-16 pl-4 w-full sm:w-1/2'>
                <section className='flex'>
                  <span className='text-accent'>&mdash;</span>
                  <a
                    className='projects lg:hover:text-accent font-bold pl-2 text-primary duration-300 transition ease-in-out'
                    href='https://quiz-wizard.netlify.app/'
                    rel='noreferrer'
                    target='_blank'>
                    Quiz Wizard - WIP
                    <span className='arrow' />
                  </a>
                </section>
                <ul>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Build with <span className='font-bold'>React (CRA)</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p>
                        Styled with <span className='font-bold'>material-ui</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Tested with <span className='font-bold'>React Testing Library</span> (integration)
                      </p>
                    </section>
                  </li>
                </ul>
              </li>
              <li className='pb-16 pl-4 w-full sm:w-1/2'>
                <section className='flex'>
                  <span className='text-accent'>&mdash;</span>
                  <a
                    className='projects lg:hover:text-accent font-bold pl-2 text-primary duration-300 transition ease-in-out'
                    href='https://www.sylwester.be'
                    rel='noreferrer'
                    target='_blank'>
                    Sylwester Portfolio
                    <span className='arrow' />
                  </a>
                </section>
                <ul>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Build with <span className='font-bold'>React (CRA)</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Styled with <span className='font-bold'>tailwindcss</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        <span className='font-bold'>Formik</span> integration with Netlify Forms
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        <span className='font-bold'>GIT LFS</span> integration with Netlify Large Media
                      </p>
                    </section>
                  </li>
                </ul>
              </li>
              <li className='pb-16 pl-4 w-full sm:w-1/2'>
                <section className='flex'>
                  <span className='text-accent'>&mdash;</span>
                  <a
                    className='projects lg:hover:text-accent font-bold pl-2 text-primary duration-300 transition ease-in-out'
                    href='https://www.christianvuye.net'
                    rel='noreferrer'
                    target='_blank'>
                    Christian Vuye Portfolio
                    <span className='arrow' />
                  </a>
                </section>
                <ul>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Build with <span className='font-bold'>React (CRA)</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        Styled with <span className='font-bold'>styled components</span>
                      </p>
                    </section>
                  </li>
                  <li className='pl-4'>
                    <section className='flex inline-block pl-2'>
                      <span className='pr-2 text-accent'>&middot;</span>
                      <p className='text-primary'>
                        <span className='font-bold'>Sanity</span> integration as headless CMS
                      </p>
                    </section>
                  </li>
                </ul>
              </li>
            </ul>
          </section>
        </section>
        <hr className='border-1 border-accent' />
      </article>
      <article className='pb-10'>
        <h2 className='font-title text-2xl pb-4 text-primary uppercase'>experience</h2>
        <ul className='py-6'>
          <li className='pl-4'>
            <span className='text-accent'>&mdash;</span>
            <a
              className='projects lg:hover:text-accent pl-2 text-primary duration-300 transition ease-in-out'
              href='https://pau.be/'
              rel='noreferrer'
              target='_blank'>
              Pàu
              <span className='arrow' />
            </a>
          </li>
        </ul>
        <hr className='border-1 border-accent' />
      </article>
      <article className='pb-10'>
        <h2 className='font-title text-2xl pb-4 text-primary uppercase'>education</h2>
        <ul className='py-6'>
          <li className='pl-4'>
            <section className='flex inline-block'>
              <span className='pr-2 text-accent'>&mdash;</span>
              <p className='text-primary'>
                Bachelor&apos;s Degree <span className='font-bold'>Audiovisual Technology: Film-Television-Video</span>{' '}
                at <span className='font-bold'>LUCA School of Arts</span>
              </p>
            </section>
          </li>
        </ul>
        <hr className='border-1 border-accent' />
      </article>
    </main>
  </Layout>
);

export default About;
