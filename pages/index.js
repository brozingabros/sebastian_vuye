import Head from 'next/head';
import Link from 'next/link';
import Layout from '../components/Layout';

const Home = () => (
  <Layout home>
    <Head>
      <title>SV - Home</title>
    </Head>

    <main className='items-center flex flex-col h-1/2 justify-center sm:h-auto'>
      <Link href='/about'>
        <a className='projects lg:hover:text-accent font-body text-2xl my-1 text-center text-primary duration-300 transition ease-in-out'>
          Front-End Developer
          <span className='arrow' />
        </a>
      </Link>
      <a
        className='projects lg:hover:text-accent font-body text-2xl my-1 text-center text-primary duration-300 transition ease-in-out'
        href='https://www.sylwester.be'
        rel='noreferrer'
        target='_blank'>
        Photographer
        <span className='arrow' />
      </a>
    </main>
  </Layout>
);

export default Home;
