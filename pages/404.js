import Head from 'next/head';
import avatar from '../constants';

const ErrorPage = () => (
  <>
    <Head>
      <title>SV - Page not Found</title>
    </Head>
    <main className='items-center flex flex-col h-screen justify-center w-full'>
      <img
        src={avatar.image.placeholder.default.trace}
        alt={avatar.image.placeholder.alt}
        className='rounded-full ml-auto mr-auto w-28 sm:w-40'
      />
      <h1 className='font-title text-5xl font-bold my-4 text-center text-accent uppercase sm:text-7xl'>
        404 error
        <span className='block text-lg my-2'>Page not found</span>
      </h1>
    </main>
  </>
);

export default ErrorPage;
