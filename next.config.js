const withOptimizedImages = require('next-optimized-images');

module.exports = withOptimizedImages({
  imageTrace: {
    color: '#CA6'
  },
  removeOriginalExtension: true
});
