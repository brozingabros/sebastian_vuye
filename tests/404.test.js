import React from 'react';
import { render, screen } from '@testing-library/react';
import ErrorPage from '../pages/404';

describe('404 page', () => {
  test('renders logo image', () => {
    render(<ErrorPage />);
    expect(screen.getByRole('img', { name: /traced placeholder avatar of sebastian vuye/i }));
  });
  test('renders heading', () => {
    render(<ErrorPage />);
    expect(screen.getByRole('heading', { name: /404 error page not found/i }));
  });
});
