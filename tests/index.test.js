import React from 'react';
import { render, screen } from '@testing-library/react';
import Home from '../pages';

describe('Home page', () => {
  test('renders logo image', () => {
    render(<Home />);
    expect(screen.getByAltText('Traced placeholder avatar of Sebastian Vuye')).toBeInTheDocument();
    expect(screen.getByAltText('Avatar of Sebastian Vuye')).toBeVisible();
  });
  test('renders heading', () => {
    render(<Home />);
    expect(screen.getByRole('heading', { name: /sebastian vuye/i })).toBeVisible();
  });
  test('renders main navigation', () => {
    render(<Home />);
    expect(screen.getByRole('link', { name: /front-end developer/i })).toBeVisible();
    expect(screen.getByRole('link', { name: /photographer/i })).toBeVisible();
  });
  test('renders footer links', () => {
    render(<Home />);
    expect(screen.getByRole('link', { name: /bitbucket/i })).toBeVisible();
    expect(screen.getByRole('link', { name: /instagram/i })).toBeVisible();
    expect(screen.getByRole('link', { name: /linkedin/i })).toBeVisible();
  });
  test('renders footer text', () => {
    render(<Home />);
    expect(screen.getByText(/made with in antwerp/i)).toBeVisible();
    expect(screen.getByText(/powered by next.js and tailwindcss/i)).toBeVisible();
    expect(screen.getByText(/© sebastian vuye/i)).toBeVisible();
  });
  test('renders footer divider dashes', () => {
    render(<Home />);
    const dividers = screen.getAllByText(/-/i);
    dividers.map(divider => expect(divider).toBeVisible());
  });
  test('renders footer divider dots', () => {
    render(<Home />);
    const dividers = screen.getAllByText(/·/i);
    dividers.map(divider => expect(divider).toBeVisible());
  });
  test('renders footer icon', () => {
    render(<Home />);
    expect(screen.getByRole('img', { name: /Golden heart icon/i })).toBeVisible();
  });
});
