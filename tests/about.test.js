import React from 'react';
import { render, screen } from '@testing-library/react';
import About from '../pages/about';

describe('About page', () => {
  test('renders logo image', () => {
    render(<About />);
    expect(screen.getByAltText('Traced placeholder avatar of Sebastian Vuye')).toBeInTheDocument();
    expect(screen.getByAltText('Avatar of Sebastian Vuye')).toBeVisible();
  });

  test('renders heading', () => {
    render(<About />);
    expect(screen.getByRole('heading', { name: /sebastian vuye/i }));
  });

  test('renders title heading', () => {
    render(<About />);
    expect(screen.getByRole('heading', { name: /about/i })).toBeVisible();
  });

  test('renders subtitle headings', () => {
    render(<About />);
    expect(screen.getByRole('heading', { name: /hello/i })).toBeVisible();
    expect(screen.getByRole('heading', { name: /used technologies/i })).toBeVisible();
    expect(screen.getByRole('heading', { name: /projects/i })).toBeVisible();
    expect(screen.getByRole('heading', { name: /main/i })).toBeVisible();
    expect(screen.getByRole('heading', { name: /side/i })).toBeVisible();
    expect(screen.getByRole('heading', { name: /experience/i })).toBeVisible();
    expect(screen.getByRole('heading', { name: /education/i })).toBeVisible();
  });

  test('renders introduction text', () => {
    render(<About />);
    expect(
      screen.getByText(
        /my name is sebastian and i’m a front-end developer. i build and deploy web interfaces from the ground up. i’m a hard worker; and love learning and researching new technologies. currently; my main focus is javascript and react. the past two years; i have worked as a consultant in web development. in my spare time, i am always working on interesting projects on the side. currently working as a consultant at/i
      )
    ).toBeVisible();
  });

  test('renders projects text', () => {
    render(<About />);

    expect(
      screen.getByText((content, node) => {
        const hasText = text =>
          text.textContent ===
          `—Webmaster at Telenet - BASE 02/2020 - now·Implementation of innovative ideas and customer driven enhancements in Adobe Experience Manager.·Setting up A/B tests in Adobe Target.·Guiding and coaching the BASE authoring squad.`;
        const nodeHasText = hasText(node);

        const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));

        return nodeHasText && childrenDontHaveText;
      })
    ).toBeVisible();

    expect(
      screen.getByText((content, node) => {
        const hasText = text =>
          text.textContent ===
          `—Webmaster at Colruyt Group - Collishop 04/2019 - 02/2020·Maintaining day-to-day operations at the Collishop website.·Automating author processes by creating templates and custom gulp builds·Converting marketing campaigns to digital landing pages in HTML, SCSS and JavaScript·Creating assets in Figma or Sketch`;
        const nodeHasText = hasText(node);

        const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));

        return nodeHasText && childrenDontHaveText;
      })
    ).toBeVisible();

    expect(
      screen.getByText((content, node) => {
        const hasText = text =>
          text.textContent ===
          `—Weather App - WIP·Build with React (CRA)·Styled with material-ui·Tested with React Testing Library (integration)`;
        const nodeHasText = hasText(node);

        const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));

        return nodeHasText && childrenDontHaveText;
      })
    ).toBeVisible();

    expect(
      screen.getByText((content, node) => {
        const hasText = text =>
          text.textContent ===
          `—Quiz Wizard - WIP·Build with React (CRA)·Styled with material-ui·Tested with React Testing Library (integration)`;
        const nodeHasText = hasText(node);

        const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));

        return nodeHasText && childrenDontHaveText;
      })
    ).toBeVisible();

    expect(
      screen.getByText((content, node) => {
        const hasText = text =>
          text.textContent ===
          `—Sylwester Portfolio·Build with React (CRA)·Styled with tailwindcss·Formik integration with Netlify Forms·GIT LFS integration with Netlify Large Media`;
        const nodeHasText = hasText(node);

        const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));

        return nodeHasText && childrenDontHaveText;
      })
    ).toBeVisible();

    expect(
      screen.getByText((content, node) => {
        const hasText = text =>
          text.textContent ===
          `—Christian Vuye Portfolio·Build with React (CRA)·Styled with styled components·Sanity integration as headless CMS`;
        const nodeHasText = hasText(node);

        const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));

        return nodeHasText && childrenDontHaveText;
      })
    ).toBeVisible();
  });

  test('renders experience text', () => {
    render(<About />);
    expect(screen.getByRole('link', { name: /pàu/i })).toBeVisible();
  });

  test('renders education text', () => {
    render(<About />);

    expect(
      screen.getByText((content, node) => {
        const hasText = text =>
          text.textContent === "Bachelor's Degree Audiovisual Technology: Film-Television-Video at LUCA School of Arts";
        const nodeHasText = hasText(node);
        const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));

        return nodeHasText && childrenDontHaveText;
      })
    ).toBeVisible();
  });

  test('renders footer text', () => {
    render(<About />);
    expect(screen.getByText(/made with in antwerp/i)).toBeVisible();
    expect(screen.getByText(/powered by next.js and tailwindcss/i)).toBeVisible();
    expect(screen.getByText(/© sebastian vuye/i)).toBeVisible();
  });
  test('renders footer divider dashes', () => {
    render(<About />);
    const dividers = screen.getAllByText(/-/i);
    dividers.map(divider => expect(divider).toBeVisible());
  });
  test('renders footer divider dots', () => {
    render(<About />);
    const dividers = screen.getAllByText(/·/i);
    dividers.map(divider => expect(divider).toBeVisible());
  });
  test('renders footer icon', () => {
    render(<About />);
    expect(screen.getByRole('img', { name: /Golden heart icon/i })).toBeVisible();
  });
});
