module.exports = {
  bail: true,
  verbose: true,
  collectCoverageFrom: ['**/*.{js,jsx,ts,tsx}', '!**/node_modules/**'],
  setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
  testPathIgnorePatterns: ['/node_modules/', '/.next/'],
  transform: {
    '^.+.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest'
  },
  transformIgnorePatterns: ['/node_modules/', '^.+.module.(css|sass|scss)$', '^.+.(css|sass|scss)$'],
  moduleNameMapper: {
    '^.+.module.(css|sass|scss)$': 'identity-obj-proxy',
    '^.+.(css|sass|scss)$': 'identity-obj-proxy',
    '^.*(jpe?g|png).*$': '<rootDir>/assetsTransformer.js'
  }
};
