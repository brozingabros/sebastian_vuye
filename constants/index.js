const profileImageHome = require(`../images/profile.jpg?sizes[]=112,sizes[]=224,sizes[]=320`);
const profileImageHomeWebP = require(`../images/profile.jpg?sizes[]=112,sizes[]=224,sizes[]=320&format=webp`);
const profileImageAlternative = require(`../images/profile.jpg?sizes[]=40,sizes[]=56,sizes[]=80,sizes[]=112`);
const profileImageAlternativeWebP = require(`../images/profile.jpg?sizes[]=40,sizes[]=56,sizes[]=80,sizes[]=112&format=webp`);
const profileImageTrace = require(`../images/profile.jpg?trace`);

const avatar = {
  alt: 'Avatar of Sebastian Vuye',
  image: {
    home: {
      default: profileImageHome,
      webP: profileImageHomeWebP
    },
    alternative: {
      default: profileImageAlternative,
      webP: profileImageAlternativeWebP
    },
    placeholder: {
      default: profileImageTrace,
      alt: 'Traced placeholder avatar of Sebastian Vuye'
    }
  }
};

export default avatar;
