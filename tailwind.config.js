module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  purge: ['./pages/**/*.js', './components/**/*.js'],
  darkMode: false,
  theme: {
    fontFamily: {
      body: ['Cabin', 'sans-serif'],
      title: ['Roboto Condensed', 'sans-serif']
    },
    extend: {
      colors: {
        primary: {
          DEFAULT: '#000'
        },
        secondary: {
          DEFAULT: '#FFF'
        },
        accent: {
          DEFAULT: '#CA6'
        }
      },
      width: {
        95: '95%',
        60: '60%'
      },
      flex: {
        '10a': '1 0 auto'
      },
      maxWidth: {
        95: '95%',
        60: '60%'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
